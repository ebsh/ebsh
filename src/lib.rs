pub fn add(left: usize, right: usize) -> usize {
    left + right
}

#[cfg(test)]
#[path = "./lib_test.rs"]
mod lib_test;
