# ebsh

Ellobium shell, aka "ebb shell", aka "E.B. shell", aka "E.B.S.H."

## License and resources

Ebsh is licensed under the CC0 (see license).

This repository also contains OpenBSD's updated `pdksh` source, which is similarly public domain. This copy of `pdksh` is only included for reference and does not compile as-is. See `ksh_upstream.txt` and `ksh_log.txt` for details on the revision the `ksh` is pulled from.

Additionally, the `pdksh` source has been ported by others to a generic build system from the OpenBSD specific build system, and to more operating systems as `oksh`. This source is not referenced for the Rust port at all, but `oksh` is used to test this implementation against on non-OpenBSD platforms.

https://github.com/ibara/oksh

## Status

Nothing useful or interesting has been implemented yet.
